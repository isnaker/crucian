unit readerRemoving;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFormRemoveReader = class(TForm)
    ComboBoxReadersList: TComboBox;
    ButtonRemoveReader: TButton;
    Label1: TLabel;
    ButtonCancelReaderRemoving: TButton;
    procedure ButtonCancelReaderRemovingClick(Sender: TObject);
    procedure ButtonRemoveReaderClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

   procedure fillReaders();

var
  FormRemoveReader: TFormRemoveReader;

implementation

uses Unit1;

{$R *.dfm}
// ���������� ����������� ������ ������� � ������������ ���������
procedure fillReaders();
var i:integer;
begin
FormRemoveReader.ComboBoxReadersList.Clear;
   for i:=0 to length(Mreaders)-1 do
   if (MReaders[i].Num > 0) then
   begin
     FormRemoveReader.ComboBoxReadersList.Items.Add( inttostr(Mreaders[i].Num) +
      ' ' + Mreaders[i].Name );
   end;
end;

procedure TFormRemoveReader.ButtonCancelReaderRemovingClick(
  Sender: TObject);
begin
FormRemoveReader.Close();
end;

procedure TFormRemoveReader.ButtonRemoveReaderClick(Sender: TObject);
var id, dlg_answer: integer;
begin
id:= ComboBoxReadersList.ItemIndex;

if (id <> -1) then
begin
dlg_answer := MessageDlg('�� ������������� ������ ������� �������� '+ Mreaders[id].Name + ' ?',mtConfirmation,mbOKCancel,0);

if (dlg_answer = 1) then
begin
removeReader(id);
FormRemoveReader.Close();
end;
end;

end;

end.
