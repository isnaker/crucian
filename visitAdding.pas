unit visitAdding;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TFormAddVisit = class(TForm)
    newVisitBookname: TEdit;
    newVisitDayscount: TEdit;
    newVisitReaderNum: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CreateNewVisitButton: TButton;
    CloseAddVisitButton: TButton;
    newVisitDate: TDateTimePicker;
    procedure CloseAddVisitButtonClick(Sender: TObject);
    procedure CreateNewVisitButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    procedure fillReadersNums(nums: array of integer);
var
  FormAddVisit: TFormAddVisit;

implementation

uses Unit1;

{$R *.dfm}

procedure fillReadersNums(nums: array of integer);
var i: integer;
begin
FormAddVisit.newVisitReaderNum.Clear;
if (length(nums) > 0) then FormAddVisit.newVisitReaderNum.text:=inttostr(nums[0]);
  for i:=0 to length(nums)-1 do
  begin
    FormAddVisit.newVisitReaderNum.Items.Add(inttostr(nums[i])) ;
  end;
end;

// ������������ ���������� ���� visit � ������� ��� �� ����������
// � �������� ������ - � ������� addNewVisit
procedure addVisit();
var visit : TBiblio;
begin
   visit.Num := strtoint(FormAddVisit.newVisitReaderNum.Text);
   visit.Visited:= DateToStr(FormAddVisit.newVisitDate.date);
   visit.Book:= FormAddVisit.newVisitBookname.Text;
   visit.DaysCount:=strtoint(FormAddVisit.newVisitDayscount.text);

   // validate data
   //
   addNewVisit(visit);
   FormAddVisit.Close;
end;

procedure TFormAddVisit.CloseAddVisitButtonClick(Sender: TObject);
begin
  FormAddVisit.Close;
end;

procedure TFormAddVisit.CreateNewVisitButtonClick(Sender: TObject);
begin
addVisit();
end;

procedure TFormAddVisit.FormCreate(Sender: TObject);
begin
newVisitDate.Date := Now();
end;

end.
