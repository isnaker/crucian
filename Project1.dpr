program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  readerAdding in 'readerAdding.pas' {FormAddReader},
  visitAdding in 'visitAdding.pas' {FormAddVisit},
  readerRemoving in 'readerRemoving.pas' {FormRemoveReader};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '1035_GuskovAO_Variant6';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TFormAddReader, FormAddReader);
  Application.CreateForm(TFormAddVisit, FormAddVisit);
  Application.CreateForm(TFormRemoveReader, FormRemoveReader);
  Application.Run;
end.
