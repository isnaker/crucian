object Form1: TForm1
  Left = 357
  Top = 160
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1050#1086#1085#1090#1088#1086#1083#1100#1085#1072#1103' '#1088#1072#1073#1086#1090#1072
  ClientHeight = 323
  ClientWidth = 1033
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object vReadersData: TStringGrid
    Left = 8
    Top = 16
    Width = 857
    Height = 209
    ColCount = 7
    DefaultColWidth = 80
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSizing, goRowSelect]
    TabOrder = 0
    ColWidths = (
      161
      129
      142
      87
      95
      143
      70)
  end
  object ButtonfindDeptor: TButton
    Left = 128
    Top = 248
    Width = 129
    Height = 25
    Caption = #1044#1086#1083#1078#1085#1080#1082#1080' '#1079#1072' '#1076#1072#1090#1091
    TabOrder = 1
    OnClick = ButtonfindDeptorClick
  end
  object ReportDayDatetimePicker: TDateTimePicker
    Left = 8
    Top = 248
    Width = 113
    Height = 25
    Date = 41633.019988020830000000
    Time = 41633.019988020830000000
    TabOrder = 2
  end
  object buildReadersListButton: TButton
    Left = 336
    Top = 248
    Width = 185
    Height = 33
    Caption = #1042#1089#1077' '#1076#1072#1085#1085#1099#1077
    TabOrder = 3
    OnClick = buildReadersListButtonClick
  end
  object GroupBox1: TGroupBox
    Left = 872
    Top = 16
    Width = 153
    Height = 89
    Caption = '   '#1057#1086#1088#1090#1080#1088#1086#1074#1082#1072' '#1095#1080#1090#1072#1090#1077#1083#1077#1081'   '
    TabOrder = 4
    object RadioButton1: TRadioButton
      Left = 16
      Top = 32
      Width = 113
      Height = 17
      Caption = #1055#1086' '#1074#1086#1079#1088#1072#1089#1090#1072#1085#1080#1102
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 16
      Top = 56
      Width = 89
      Height = 17
      Caption = #1055#1086' '#1091#1073#1099#1074#1072#1085#1080#1102
      TabOrder = 1
      OnClick = RadioButton2Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 872
    Top = 120
    Width = 153
    Height = 89
    Caption = '   '#1057#1086#1088#1090#1080#1088#1086#1074#1082#1072' '#1087#1086#1089#1077#1097#1077#1085#1080#1081'   '
    TabOrder = 5
    object RadioButton3: TRadioButton
      Left = 16
      Top = 32
      Width = 113
      Height = 17
      Caption = #1055#1086' '#1074#1086#1079#1088#1072#1089#1090#1072#1085#1080#1102
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton3Click
    end
    object RadioButton4: TRadioButton
      Left = 16
      Top = 56
      Width = 89
      Height = 17
      Caption = #1055#1086' '#1091#1073#1099#1074#1072#1085#1080#1102
      TabOrder = 1
      OnClick = RadioButton4Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 576
    Top = 232
    Width = 449
    Height = 65
    Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1103#1084#1080
    TabOrder = 6
    object ButtonRemoveReader: TButton
      Left = 302
      Top = 24
      Width = 131
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1095#1080#1090#1072#1090#1077#1083#1103
      TabOrder = 0
      OnClick = ButtonRemoveReaderClick
    end
    object ButtonShowAddReaderForm: TButton
      Left = 16
      Top = 24
      Width = 131
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1095#1080#1090#1072#1090#1077#1083#1103
      TabOrder = 1
      OnClick = ButtonShowAddReaderFormClick
    end
    object ButtonAddVisit: TButton
      Left = 160
      Top = 24
      Width = 129
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1073#1088#1072#1097#1077#1085#1080#1077
      TabOrder = 2
      OnClick = ButtonAddVisitClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 304
    Width = 1033
    Height = 19
    Panels = <>
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 184
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N6: TMenuItem
        Caption = #1047#1072#1076#1072#1085#1080#1077
        OnClick = N6Click
      end
      object N2: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080' '#1074#1099#1081#1090#1080
        OnClick = N2Click
      end
    end
    object N3: TMenuItem
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      object N7: TMenuItem
        Caption = #1042#1089#1077' '#1076#1072#1085#1085#1099#1077
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1095#1080#1090#1072#1090#1077#1083#1103
        OnClick = N8Click
      end
      object N9: TMenuItem
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1073#1088#1072#1097#1077#1085#1080#1077
        OnClick = N9Click
      end
      object N16: TMenuItem
        Caption = #1059#1076#1072#1083#1080#1090#1100' '#1095#1080#1090#1072#1090#1077#1083#1103
        OnClick = N16Click
      end
      object N4: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1089#1086#1076#1077#1088#1078#1080#1084#1086#1077' '#1092#1072#1081#1083#1086#1074
        OnClick = N4Click
      end
    end
    object N5: TMenuItem
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      object N10: TMenuItem
        Caption = #1063#1080#1090#1072#1090#1077#1083#1080':'
        Enabled = False
      end
      object N11: TMenuItem
        Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086' '#1074#1086#1079#1088#1072#1089#1090#1072#1085#1080#1102
        OnClick = N11Click
      end
      object N12: TMenuItem
        Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086' '#1091#1073#1099#1074#1072#1085#1080#1102
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = #1055#1086#1089#1077#1097#1077#1085#1080#1103':'
        Enabled = False
        RadioItem = True
      end
      object N14: TMenuItem
        Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086' '#1074#1086#1079#1088#1072#1089#1090#1072#1085#1080#1102
        OnClick = N14Click
      end
      object N15: TMenuItem
        Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086' '#1091#1073#1099#1074#1072#1085#1080#1102
        OnClick = N15Click
      end
    end
  end
end
