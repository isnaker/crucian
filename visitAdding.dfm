object FormAddVisit: TFormAddVisit
  Left = 351
  Top = 205
  Width = 391
  Height = 343
  Caption = #1053#1086#1074#1086#1077' '#1086#1073#1088#1072#1097#1077#1085#1080#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 40
    Width = 149
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1095#1080#1090#1072#1090#1077#1083#1100#1089#1082#1086#1075#1086' '#1073#1080#1083#1077#1090#1072
  end
  object Label2: TLabel
    Left = 24
    Top = 80
    Width = 86
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1089#1077#1097#1077#1085#1080#1103
  end
  object Label3: TLabel
    Left = 24
    Top = 112
    Width = 82
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1082#1085#1080#1075#1080
  end
  object Label4: TLabel
    Left = 24
    Top = 152
    Width = 61
    Height = 13
    Caption = #1050#1086#1083'-'#1074#1086' '#1076#1085#1077#1081
  end
  object newVisitBookname: TEdit
    Left = 192
    Top = 104
    Width = 161
    Height = 21
    TabOrder = 0
    Text = 'newVisitBookname'
  end
  object newVisitDayscount: TEdit
    Left = 192
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '3'
  end
  object newVisitReaderNum: TComboBox
    Left = 192
    Top = 40
    Width = 65
    Height = 21
    ItemHeight = 13
    TabOrder = 2
  end
  object CreateNewVisitButton: TButton
    Left = 24
    Top = 264
    Width = 123
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100
    TabOrder = 3
    OnClick = CreateNewVisitButtonClick
  end
  object CloseAddVisitButton: TButton
    Left = 232
    Top = 264
    Width = 123
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 4
    OnClick = CloseAddVisitButtonClick
  end
  object newVisitDate: TDateTimePicker
    Left = 192
    Top = 72
    Width = 113
    Height = 21
    Date = 41632.528557511580000000
    Time = 41632.528557511580000000
    TabOrder = 5
  end
end
