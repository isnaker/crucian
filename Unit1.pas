unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ComCtrls, Menus;

type
  TForm1 = class(TForm)
    vReadersData: TStringGrid;
    ButtonfindDeptor: TButton;
    ReportDayDatetimePicker: TDateTimePicker;
    buildReadersListButton: TButton;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N6: TMenuItem;
    N4: TMenuItem;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox2: TGroupBox;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    N5: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    GroupBox3: TGroupBox;
    ButtonRemoveReader: TButton;
    ButtonShowAddReaderForm: TButton;
    ButtonAddVisit: TButton;
    StatusBar1: TStatusBar;
    N16: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ButtonShowAddReaderFormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonAddVisitClick(Sender: TObject);
    procedure ButtonfindDeptorClick(Sender: TObject);
    procedure buildReadersListButtonClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure ButtonRemoveReaderClick(Sender: TObject);
    procedure N16Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;


type TReaders = record

  Num : integer;
  Name : string[127];
  Address: string[255];
  Phone: string[31];
end;

type TBiblio = record

  Num : integer;
  Visited : string[79];
  Book : string[255];
  DaysCount : integer;
end;

procedure addNewReader(reader: TReaders);
procedure addNewVisit(visit: Tbiblio);
procedure removeReader(readerID : integer);

var
  Mreaders : array of TReaders;
  Mbiblio : array of TBiblio;

var
  Form1: TForm1;

implementation

uses Unit2, readerAdding, visitAdding, DateUtils, readerRemoving;

var readers : file of TReaders;
var biblio  : file of TBiblio;

{$R *.dfm}

// ��������� ����������� �������� ��� stringgrid
procedure setDefaultGridView();
begin
    // string grid cells data binding
with (Form1.vReadersData) do
begin
    Cells[0,0] := '����� ������������� ������';
    Cells[1,0] := '�������';
    Cells[2,0] := '�����';
    Cells[3,0] := '�������';
    // biblio data
    Cells[4,0] := '���� ���������';
    Cells[5,0] := '�����';
    Cells[6,0] := '���-�� ����';
end;
end;

// ���������� ������� � ���������. ����� - �����������
procedure sortReaders(var readers:array of TReaders; DESCdirection : boolean);
var j,n,i : integer;
    t: TReaders;
    flagsort: boolean;
begin

n := length(readers)-1;

if not(DESCdirection) then
begin
repeat
    flagsort:=true;
    for i:=0 to n-1 do
      if not(readers[i].num <= readers[i+1].num) then begin
        t:=readers[i];
        readers[i]:=readers[i+1];
        readers[i+1]:=t;
        j:=i;

        while (j>1)and not(readers[j-1].num <= readers[j].num) do begin
          t:=readers[j];
          readers[j]:=readers[j-1];
          readers[j-1]:=t;
          dec(j);
        end;
        flagsort:=false;
      end;
  until flagsort;
end

else

begin
repeat
    flagsort:=true;
    for i:=0 to n-1 do
      if not(readers[i].num >= readers[i+1].num) then begin
        t:=readers[i];
        readers[i]:=readers[i+1];
        readers[i+1]:=t;
        j:=i;

        while (j>1)and not(readers[j-1].num >= readers[j].num) do begin
          t:=readers[j];
          readers[j]:=readers[j-1];
          readers[j-1]:=t;
          dec(j);
        end;
        flagsort:=false;
      end;
  until flagsort;
end;

end;

// ���������� ������� � ����������. ����� - �����������
procedure sortBiblio(var biblio: array of TBiblio; DESCdirection : boolean);
var j,n,i : integer;
    t: TBiblio;
    flagsort: boolean;
begin

n := length(biblio)-1;

if not(DESCdirection) then
begin
repeat
    flagsort:=true;
    for i:=0 to n-1 do
      if not( StrToDate(biblio[i].Visited) <= StrToDate(biblio[i+1].Visited)) then begin
        t:=biblio[i];
        biblio[i]:=biblio[i+1];
        biblio[i+1]:=t;
        j:=i;

        while (j>1)and not( StrToDate(biblio[j-1].Visited) <= StrToDate(biblio[j].Visited)) do begin
          t:=biblio[j];
          biblio[j]:=biblio[j-1];
          biblio[j-1]:=t;
          dec(j);
        end;
        flagsort:=false;
      end;
  until flagsort;
end

else

begin
repeat
    flagsort:=true;
    for i:=0 to n-1 do
      if not( StrToDate(biblio[i].Visited) >= StrToDate(biblio[i+1].Visited)) then begin
        t:=biblio[i];
        biblio[i]:=biblio[i+1];
        biblio[i+1]:=t;
        j:=i;

        while (j>1)and not( StrToDate(biblio[j-1].Visited) >= StrToDate(biblio[j].Visited)) do begin
          t:=biblio[j];
          biblio[j]:=biblio[j-1];
          biblio[j-1]:=t;
          dec(j);
        end;
        flagsort:=false;
      end;
  until flagsort;
end


end;

// ���������������� �������
procedure ini();
var tempReader: TReaders;
    tempBiblio: TBiblio;
begin
SetLength(Mreaders,0);
SetLength(Mbiblio,0);
// check file 1
assign(readers, 'readers.txt');

try
  reset(readers);
Except
  try
  Rewrite(readers);
  except
  ShowMessage('������ - ���� �� ������.');
  end;
end;

// check file 2
assign(biblio, 'biblio.txt');
try
  reset(biblio);
Except
  Rewrite(biblio);
end;

// Fill arrays with data
  While not (Eof(readers)) do
  begin
    Read(readers, tempReader);
    SetLength(Mreaders, length(Mreaders)+1);
    Mreaders[length(Mreaders)-1]:= tempReader;
  end;

  While not (Eof(biblio)) do
  begin
    Read(biblio, tempBiblio);
    SetLength(Mbiblio, length(Mbiblio)+1);
    Mbiblio[length(Mbiblio)-1]:= tempBiblio;
  end;

  // sorting
   sortBiblio(Mbiblio, false);
   sortReaders(Mreaders, false);
end;


// �������� �������
procedure clearOutput();
var i: integer;
begin
  for i:=1 to form1.vReadersData.RowCount do
  form1.vReadersData.Rows[i].Clear;
end;

// ����� ���� ��������� �� ����� ������� �������
procedure buildReadersList();
var i,j, rowCounter: integer;
begin
    rowCounter:=0;
    form1.vReadersData.RowCount := length(Mreaders)+length(Mbiblio)+2;
    form1.vReadersData.ColCount := 7;
    form1.vReadersData.FixedRows:=1;

    setDefaultGridView();
    clearOutput();
   for i:=1 to length(Mreaders) do
   if (Mreaders[i-1].Num > 0) then
   begin
      with (Form1.vReadersData) do
      begin
        inc(rowCounter);
        Cells[0,rowCounter]:= inttostr(Mreaders[i-1].num);
        Cells[1,rowCounter]:= Mreaders[i-1].Name;
        Cells[2,rowCounter]:= Mreaders[i-1].Address;
        Cells[3,rowCounter]:= Mreaders[i-1].Phone;

        for j:=0 to length(Mbiblio)-1 do
        begin
          if (Mbiblio[j].num = Mreaders[i-1].num) then
          begin
            inc(rowCounter);
            Cells[4,rowCounter]:= Mbiblio[j].Visited;
            Cells[5,rowCounter]:= Mbiblio[j].Book;
            Cells[6,rowCounter]:= inttostr(Mbiblio[j].DaysCount);
          end;

        end;

      end;
   end;
end;

//���������� ������ ��������
procedure addNewReader(reader: TReaders);
var newLength: integer;
begin
        newLength :=  length(Mreaders) +1;
        SetLength(Mreaders, newLength );
        Mreaders[length(Mreaders)-1]:=reader;
        buildReadersList;
end;

// ���������� ������ ���������
procedure addNewVisit(visit: Tbiblio);
var newLength: integer;
begin
        newLength :=  length(Mbiblio) +1;
        SetLength(Mbiblio, newLength );
        Mbiblio[length(Mbiblio)-1]:=visit;
        buildReadersList;
end;


// ��������� ������� ������ ��� ��������
procedure removeReader(readerID : integer);
var reader : TReaders;
    i : integer;
begin
  reader:= Mreaders[readerID];

  For i:=0 to length(Mbiblio)-1 do
  if (Mbiblio[i].Num = reader.Num) then MBiblio[i].Num := 0;

  Mreaders[readerID].Num:=0;
end;


// ���������� ���� ��������� � �����
procedure saveChanges();
var i, readersCount, biblioCount: integer;
begin
readersCount:=0;
biblioCount :=0;

  // readers
 Rewrite(readers);
 For i:=0 to length(Mreaders)-1 do
 if (Mreaders[i].Num > 0) then
 begin
  Seek(readers,readersCount);
  inc(readersCount);
  Write(readers, Mreaders[i]);
 end;

 // visits
  Rewrite(biblio);
 For i:=0 to length(Mbiblio)-1 do
 if (Mbiblio[i].Num > 0) then
 begin
  Seek(biblio,biblioCount);
  inc(biblioCount);
  Write(biblio, Mbiblio[i]);
 end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
ini();
buildReadersList();
ReportDayDatetimePicker.Date := Now();
end;

procedure TForm1.ButtonShowAddReaderFormClick(Sender: TObject);
begin
FormAddReader.ShowModal;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
saveChanges();
closeFile(readers);
closeFile(biblio);
end;

procedure TForm1.ButtonAddVisitClick(Sender: TObject);
var nums: array of integer; i: integer;
begin

if (length(Mreaders) = 0) then
ShowMessage('������ �������� ��������� - ��� �� ������ ��������')
else

begin
for i:=1 to length(Mreaders) do
begin
SetLength(nums, i);
nums[i-1]:=Mreaders[i-1].num;
end;

fillReadersNums(nums);
FormAddVisit.ShowModal;
end;
end;

// ����� ���������
procedure findDebtor(reportDate: TDate);
var tmp_date: TDate;
var tmp_reader:TReaders; tmp_biblio:TBiblio;

  verh_reader: integer;
  niz_reader: integer;
  sred_reader: integer;
  found_reader: boolean;

  i, searchNum, rowCounter: integer;

begin
 //* TODO: Find debtor
 //* BINARY SEARCHING
 //* ���������� ����������� ��� ������ ������� �����, ����� ��������� ��
 //* �������� ���� � �� � �������������  ������  ���������� �� �.�.�. � �������.
  rowCounter:=0;

  clearOutput();
  with (Form1.vReadersData) do
  begin
  RowCount:= 2;
  ColCount:=7;
      Cells[0,0]:='�����';
      Cells[1,0]:='���� ���������';
      Cells[2,0]:='���-�� ����';
      Cells[3,0]:='���������� ����';
      Cells[4,0]:='���';
      Cells[5,0]:='�������';
      Cells[6,0]:='������';
  end;

  for i:=0 to length(mBiblio)-1 do
   begin
     tmp_biblio:=mbiblio[i];
     tmp_date:=StrToDate(tmp_biblio.Visited);
     tmp_date:= IncDay(tmp_date, tmp_biblio.DaysCount);

      if (tmp_date < reportDate) then
      begin

         searchNum:= tmp_biblio.Num;
        // find reader with **binary search
        verh_reader:=length(mreaders);
        niz_reader:=0;
        sred_reader:= trunc( (niz_reader - verh_reader)/2) + verh_reader;
         found_reader := false;
        repeat

          tmp_reader:=Mreaders[sred_reader];

          if (tmp_reader.Num = searchNum) then  // �������
            begin
              inc(rowCounter);
              found_reader := TRUE;

              with (Form1.vReadersData) do
                begin
                    RowCount:= RowCount+1;
                    Cells[0,rowCounter]:= inttostr(tmp_biblio.Num);
                    Cells[1,rowCounter]:= tmp_biblio.Visited;
                    Cells[2, rowCounter]:= inttostr(tmp_biblio.DaysCount);
                    Cells[3,rowCounter]:=  inttostr(DayOf(reportDate - strtodate(tmp_biblio.visited)) - (tmp_biblio.DaysCount-1) );
                    Cells[4,rowCounter]:= tmp_reader.Name;
                    Cells[5,rowCounter]:= tmp_reader.Phone;
                    Cells[6,rowCounter]:= '�������';
                end;
            end;

            if (tmp_reader.Num > searchNum)  then
            dec(sred_reader)
            else
            inc(sred_reader);

            until (found_reader) or (sred_reader < 0) or (sred_reader > verh_reader);

          ///////////////////
       end;
  end;
end;

procedure clearFiles();
begin
Rewrite(readers);
Rewrite(biblio);

setLength(Mreaders,0);
setLength(Mbiblio,0);

ini();
buildReadersList();

end;

// �������� �������� ����������� ���������

procedure TForm1.ButtonfindDeptorClick(Sender: TObject);
begin
sortReaders(Mreaders, false);
RadioButton1.Checked:=true;
  findDebtor(Form1.ReportDayDatetimePicker.Date);
end;

procedure TForm1.buildReadersListButtonClick(Sender: TObject);
begin
  buildReadersList();
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  form1.close();
  Application.Terminate;
end;

procedure TForm1.N7Click(Sender: TObject);
begin
  buildReadersList();
end;

procedure TForm1.N8Click(Sender: TObject);
begin
FormAddReader.ShowModal;
end;

procedure TForm1.N9Click(Sender: TObject);
var nums: array of integer; i: integer;
begin
for i:=1 to length(Mreaders) do
begin
SetLength(nums, i);
nums[i-1]:=Mreaders[i-1].num;
end;

fillReadersNums(nums);
FormAddVisit.ShowModal;
end;

procedure TForm1.N5Click(Sender: TObject);
begin
Form2.ShowModal;
end;

procedure TForm1.N6Click(Sender: TObject);
begin
Form2.show();
end;

procedure TForm1.N4Click(Sender: TObject);
begin
clearFiles();
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
sortReaders(Mreaders, false);
buildReadersList();
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
sortReaders(Mreaders, true);
buildReadersList();
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
sortBiblio(Mbiblio, false);
buildReadersList();
end;

procedure TForm1.RadioButton4Click(Sender: TObject);
begin
sortBiblio(Mbiblio, true);
buildReadersList();
end;

procedure TForm1.N11Click(Sender: TObject);
begin
sortReaders(Mreaders, false);
RadioButton1.Checked:=true;
buildReadersList();
end;

procedure TForm1.N12Click(Sender: TObject);
begin
sortReaders(Mreaders, true);
RadioButton2.Checked:=true;
buildReadersList();
end;

procedure TForm1.N14Click(Sender: TObject);
begin
sortBiblio(Mbiblio, false);
RadioButton3.Checked:=true;
buildReadersList();
end;

procedure TForm1.N15Click(Sender: TObject);
begin
sortBiblio(Mbiblio, true);
RadioButton4.Checked:=true;
buildReadersList();
end;

procedure TForm1.ButtonRemoveReaderClick(Sender: TObject);
begin
fillReaders();
FormRemoveReader.ShowModal();
end;

procedure TForm1.N16Click(Sender: TObject);
begin
fillReaders();
FormRemoveReader.ShowModal();
end;

end.
