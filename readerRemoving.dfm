object FormRemoveReader: TFormRemoveReader
  Left = 344
  Top = 270
  Width = 580
  Height = 123
  Caption = #1059#1076#1072#1083#1077#1085#1080#1077' '#1095#1080#1090#1072#1090#1077#1083#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 303
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1095#1080#1090#1072#1090#1077#1083#1103' '#1080#1079' '#1089#1087#1080#1089#1082#1072' '#1080' '#1085#1072#1078#1084#1080#1090#1077' '#1082#1085#1086#1087#1082#1091' "'#1059#1076#1072#1083#1080#1090#1100'"'
  end
  object ComboBoxReadersList: TComboBox
    Left = 24
    Top = 48
    Width = 321
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'ComboBoxReadersList'
  end
  object ButtonRemoveReader: TButton
    Left = 360
    Top = 48
    Width = 89
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 1
    OnClick = ButtonRemoveReaderClick
  end
  object ButtonCancelReaderRemoving: TButton
    Left = 464
    Top = 48
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = ButtonCancelReaderRemovingClick
  end
end
