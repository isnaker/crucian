unit readerAdding;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask;

type
  TFormAddReader = class(TForm)
    newReaderName: TEdit;
    newReaderAddress: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    newReaderPhone: TMaskEdit;
    FormAddReaderAddReaderButton: TButton;
    FormAddReaderCloseButton: TButton;
    newReaderNum: TEdit;
    procedure FormAddReaderCloseButtonClick(Sender: TObject);
    procedure FormAddReaderAddReaderButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAddReader: TFormAddReader;

implementation

uses Unit1;

{$R *.dfm}
procedure FormAddReaderClose();
begin
   FormAddReader.Close;
end;

// ������������ ���������� ���� reader � ������� ��� �� ����������
// � �������� ������ - � ������� addNewReader
procedure addReader();
var reader : TReaders;
begin
  reader.Num := strtoint(FormAddReader.newReaderNum.Text);
  reader.Name := FormAddReader.newReaderName.Text;
  reader.Address := FormAddReader.newReaderAddress.Text;
  reader.Phone := FormAddReader.newReaderPhone.Text;

  Unit1.addNewReader(reader);
  FormAddReaderClose();
end;



procedure TFormAddReader.FormAddReaderCloseButtonClick(Sender: TObject);
begin
   FormAddReaderClose();
end;

procedure TFormAddReader.FormAddReaderAddReaderButtonClick(
  Sender: TObject);
begin
  addReader();
end;

procedure TFormAddReader.FormShow(Sender: TObject);
begin
 newReaderNum.Text := inttostr(1000 + length(Mreaders) + 1);
end;

end.
